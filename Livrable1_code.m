disp("Projet risque de credit");
% simu1();
% simu2();
% simu3();
 exo1et2();
% exo3();
% exo4();
% exo5();
% exo6();

function[] = simu1()
Nmc = 10000000; % 10^7
cpt = 0;
    for i = 1 : Nmc
        if(randn > 5) 
            cpt=cpt+1;
        end
    end
    disp("Proba (X > 5)            :" + cpt/Nmc); 
end

function[] = simu2()
Nmc = 10000; %10^4
mu = 5;
cpt = 0;
A = 0;
B = 0;
C = 0;
theta = 1;
T = 1;
resultat = 0;

    for i = 1 : Nmc
        Yq(i) = randn;
        Yp(i) = mu + Yq(i);
        if(Yp(i) > 5)
           cpt = cpt+1;
           A = A + exp(-mu * Yp(i) + mu * mu * 0.5);
           B = B + exp(-mu * Yq(i) - mu * mu * 0.5);
           C = C + exp(-mu * Yp(i) * theta - theta * theta * T * 0.5);
        end
    end
    
    function[f] = indicatrice(Y)
        if(Y > 5)
            f = 1;
        else
            f = 0;
        end
    end

    for i = 1 : Nmc
        g = randn;
        Y = mu + g;
        X = indicatrice(Y) * exp(-mu * Y * theta - theta * theta * T * 0.5);
        resultat = resultat + X;              
    end
 %disp("Proba Y=mu+X > 5     :" + resultat/Nmc);
 disp("Proba (Y=mu+X > 5)       :" + cpt/Nmc);
 
 disp("Proba (X > 5) sous P     :" + A/Nmc);
 disp("Proba (X > 5) sous Q     :" + B/Nmc);

 disp("Proba avec theta?        :" + C/Nmc);
    
end

function[]= simu3()
Nmc = 10000;
T = 1;
theta = 1;
W(1) = 0;
A = 0;
    for i = 1 : Nmc
        W(i) = sqrt(T) * randn;
        A = A + (W(i) + theta * T) * exp(-W(i) * theta - theta * theta * T/2);     
    end
    
    disp("Resultat :" + A/Nmc + ", très proche de 0 :)");
end

function[] = exo1et2()
% -------------------------------------------------------------------------
    % Initialisation des variables
        S0 = 100;
        r = 0;
        sigma = 0.4; 
        Nmc = 100;
        B = 36; % à modifier (100, 50, 36)
    
    %Pour simuler la trajectoire de l'actif, discrétisons l'intervalle
    %[0,T] en N parties:
        T = 1;
        N = 100;
        delta_t = T / N;
        t = (0:N) * delta_t; %t=linspace(0,T,N+1);
    
    %Tableau pour stocker une trajectoire de l'actif (par la methode des
    %differences finies).
        S(1) = S0;
        
    %Tableau pour stocker une trajectoire de l'actif (par resolution
    %analytique).
        S2(1) = S0;
        
    %Tableau pour stocker une trajectoire du MB
        W(1) = 0;

    %Compteur pour calculer les probabilités
        cpt = 0;
        cpt2 = 0;
        
    %Tableau pour calculer la variance
        var(1) = 0;
  
% -------------------------------------------------------------------------
     for k = 1:Nmc
        % boucle pour une trajectoire du mouvement brownien
        for i = 1:N
            W(i + 1) = W(i) + sqrt(delta_t) * randn;
        %Differences Finies
            S(i + 1) = S(i) + r * S(i) * delta_t + sigma * S(i) * (W(i + 1) - W(i));
            
        %Solution analytique
            S2(i + 1) = S(i) * (exp((r - sigma * sigma/2) * delta_t + sigma * (W(i + 1) - W(i))));
        end
         
%          if S(N+1) < B 
%              plot(t,S, 'r');
%          else  
%              plot(t,S, 'b');
%          end
         
         if S2(N + 1) < B 
             plot(t,S2, 'r');
         else  
             plot(t,S2, 'b');
         end

        hold on;
        %On stock la derniere valeur de l'actif dans un tableau
            last_value(k) = S(N + 1);
            last_value2(k) = S2(N + 1);
        
        %Definition de X := St-B
            X(k) = S(N + 1) - B;
            X2(k) = S2(N + 1) - B;
        
        %Tableau pour calculer la variance
            var(k) = X(k)^2;
        
        %Calcul de la proba
        if (last_value(k) < B) 
            cpt = cpt + 1;
        end 
        
        if (last_value2(k) < B) 
            cpt2 = cpt2 + 1;
        end 
        
    end
% -------------------------------------------------------------------------
%parametres du graphique (X,Y, Legend) hors de la boucle
%pour optimiser la rapidité d'exécution
        xlabel 't'
        ylabel 'Processus S_t'
        %title 'Trajectoire de S'

        title 'Trajectoires de S, S(T)<B en rouge et S(T)>=B en bleu'
% -------------------------------------------------------------------------

%Calcul de la proba 
    proba = cpt/Nmc;
    proba2 = cpt2/Nmc;
    disp("proba " + proba);
    disp("proba2 " + proba2);
     
   % B=100 -> proba dans [60%, 70%]
   % B=50  -> proba dans [3%, 10%]
   % B=36  -> proba dans [0, 4%]
% -------------------------------------------------------------------------
 


%EXO 2- Simulation des fonctions de densité
disp("volatilité :" + sqrt(mean(var) - mean(X)^2));

a = -300;
b = 3;
delta_x = b-a/Nmc;

for i = 1 : Nmc + 1
    
    x(i) = a + delta_x * (i-1);
    cont_d = 0;
    cont_r = 0;
    
    for n = 1 : Nmc
        
       %Fonction de densite empirique
       if x(i) <= X2(n) && X2(n) <= x(i) + delta_x % changer X2 par X si besoin
           cont_d = cont_d+1;
       end
       
       %Fonction de reparition empirique
       if(X2(n) <= x(i)) % changer X2 par X si besoin
           cont_r=cont_r+1;
       end
       
    end
    
    P_d(i) = cont_d/Nmc;
    repartition(i) = cont_r/Nmc;
    densite(i) = P_d(i)/delta_x;
   
end
% -------------------------------------------------------------------------
%Fonction de densite empirique
 figure;
 plot(x,densite,'ro','MarkerSize',2,'MarkerFaceColor', 'r' );
 xlabel 'x'
 ylabel 'f_X(x)'
 title 'Fonctions de densité empirique '

% -------------------------------------------------------------------------
%Fonction de repartition
 tic;
 figure;
 plot(x,repartition,'ro','MarkerSize',2,'MarkerFaceColor', 'r' );
 xlabel 'x'
 ylabel 'F_X(x)'
 title 'Fonctions de repartition empirique '
end 


function[] = exo3()
% -------------------------------------------------------------------------
%definition de l'indicatrice de y <= z
    function[res] = phi(z,y)
        if y > z
            res = 0;
        else
            res = 1;
        end
    end
% -------------------------------------------------------------------------
    %Initialisation des variables 
        Nmc = 1000;   
        lambda = 0.9; 
        t= (0:Nmc) * 1;
    
    %Valeurs a changer
        Z0 = 1;
        beta = 1000;
 
    %Tableau pour l'algorithme
        Z(1) = Z0;
    
    for i = 1 : Nmc
        gamma(i) = beta/((i + 1)^lambda);
        Y = randn;
        Z(i + 1) = Z(i) - gamma(i) * (phi(Z(i),Y) - 1/2);
    end
     plot(t,Z);
end

function[] = exo4()
%travail 4: application algorithme Robbins-Monro
% -------------------------------------------------------------------------

    function[s] = psi(z,x) % indicatrice
       if(x <= z)
           s = 1;
       else 
           s = 0;
       end
    end
% -------------------------------------------------------------------------
    % Initialisation des variables
        r = 0;
        sigma = 0.4; 
        S0 = 100;
        Z0 = 0.1;

        Nmc = 1000000;
        lambda = 0.9;
        beta = 10;
        
    %Tableau pour l'algorithme
    Z(1) = Z0;
    
    % Valeurs a changer
        alpha = 0.1; % -> 1-alpha = proba = 0.99 = 99%
        T = 1;
        B = 50;
    
    for i = 1 : Nmc
        X(i) = S0 * exp((r - sigma * sigma/2)*(T) + sigma * sqrt(T) * randn) - B;
        gamma(i) = beta/((i + 1)^lambda);
        Z(i+1) = Z(i) - gamma(i) * (psi(Z(i),X(i)) - alpha);
    end
    disp("l'alogrithme converge vers la valeur : "+Z(Nmc + 1));
    plot(Z);
    
end

function[] = exo5()
% -------------------------------------------------------------------------
    % Initialisation des variables
        S0 = 100;
        r = 0;
        sigma = 0.4; 
        S(1) = S0;
        W(1)=0;
        Nmc = 100;
    
    %Pour simuler la trajectoire de l'actif, discrétisons l'intervalle
    %[0,T] en N parties:
        T = 1;
        N = 100;
        delta_t = T / N;
    
    %Parametres a changer
        alpha = 0.1;
        B = 50;
% -------------------------------------------------------------------------
  
        % boucle pour une trajectoire du mouvement brownien et de l'actif
        for i = 1 : N
            W(i + 1) = W(i) + sqrt(delta_t) * randn;
            S(i + 1) = S(i) + r * S(i) * delta_t + sigma * S(i) * (W(i+1) - W(i));
        end
        X = S(N + 1) - B; 
        %on verifie si l'echantillon respect la condtion X=ST-B<0
        if(X < 0)
            plot(S,'r');
            
            Y = sort(S);
            disp(Y);
            disp("X[ partie_entire(Nmc*alpha) ] = " +Y(floor(Nmc * alpha)));
        end
end

function[P,x] = fonction_Emp_densite(X,a,delta)

N_x = length(X);

    for i = 1 : N_x + 1
        x(i) = a + delta * (i-1); 
        cont = 0;
        
        for n = 1 : length(X)
            if (X(n) <= x(i) + delta && X(n) > x(i))
                cont = cont + 1;
            end
        end
        
        P(i) = cont/(length(X));
    end
end

function[] = densite_Emp_graphe(a,delta,X)
[P,x] = fonction_Emp_densite(X,a,delta);
figure;
plot(x,P,'ro','MarkerSize',1.5,'MarkerFaceColor', 'r' );
xlabel 'x'
ylabel 'f_X(x)'
title 'Fonctions de densité empirique '
end
 
function[] = exo6()
% -------------------------------------------------------------------------

    %Variables
        alpha = 5;
        beta = 1;
    
    %Constantes
        x0 = (alpha - 1)/(alpha + beta - 2);
        c = (gamma(alpha + beta)/(gamma(alpha) * gamma(beta))) * (x0^(alpha - 1)) * ((x0 - 1)^(beta - 1));  
        Nmc = 10000;
        k = 1;
        
% -------------------------------------------------------------------------
%Application de la méthode de rejet
    for n = 1 : Nmc
        y = rand;
        u = rand;
        f = (gamma(alpha + beta)/(gamma(alpha) * gamma(beta))) * y^(alpha - 1)*(y - 1)^(beta - 1);
        g = rand;
        if (u <= f/(c * g))
           X(k) = y;
           k = k + 1;
        end    
    end
% -------------------------------------------------------------------------
%Graphe de la densite empirique
    densite_Emp_graphe(-2,0.0009,X);
end